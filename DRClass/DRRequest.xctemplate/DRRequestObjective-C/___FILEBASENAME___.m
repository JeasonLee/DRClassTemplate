//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

#import "___FILEBASENAME___.h"

@implementation ___FILEBASENAMEASIDENTIFIER___ {
    
}

- (instancetype)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}

+ (___FILEBASENAMEASIDENTIFIER___ *)requestWithSuccess:(void (^)(NSString *message, NSString *statusCode))success failure:(void (^)(NSString *message))failure {
    ___FILEBASENAMEASIDENTIFIER___ *request = [[___FILEBASENAMEASIDENTIFIER___ alloc] init];
    [request startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        ___FILEBASENAMEASIDENTIFIER___ *_request = (___FILEBASENAMEASIDENTIFIER___ *)request;
        success ? success(_request.successMessage, _request.statusCode) : nil;
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        ___FILEBASENAMEASIDENTIFIER___ *_request = (___FILEBASENAMEASIDENTIFIER___ *)request;
        failure ? failure(_request.errorMessage) : nil;
    }];
    return request;
}

- (NSString *)requestUrl {
    return <#requestUrl#>;
}

- (NSString *)serCode {
    return <#serCode#>;
}

- (BOOL)needToken {
    return <#needToken#>;
}

- (id)requestArgument {
    NSMutableDictionary *dataMsg = [NSMutableDictionary dictionary];
    //[dataMsg safeSetObject:<#value#> forKey:<#key#>];
    return [self makeUpParametersWithDataMsg:dataMsg];
}

//- (<#type#>) <#callBackArgument#> {
//    return self.responseJSONObjectResult[<#key#>];
//}

@end
