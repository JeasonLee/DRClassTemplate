XCODEPATH = /Applications/Xcode.app/Contents/MacOS
XCODETEMPLATEPATH = /Applications/Xcode.app/Contents/Developer/Library/Xcode/Templates/File\ Templates
Template = DRClass

install::
	@if ! [[ $EUID -eq 0 ]]; then\
		echo "This script should be run using sudo or as the root user.";\
		exit 1;\
	fi
	@if ! [ -f "${XCODEPATH}/Xcode" ]; then\
		echo "Can not find the Xcode.";\
		exit 1;\
	fi
	@if [[ -d "${XCODETEMPLATEPATH}/${Template}" ]]; then\
		echo "DRClass file found! ";\
	else \
		cp -R ./${Template} ${XCODETEMPLATEPATH};\
		echo "Install successed!";\
	fi

uninstall::
	@if ! [[ $EUID -eq 0 ]]; then\
		echo "This script should be run using sudo or as the root user.";\
    	exit 1;\
	fi
	@if ! [ -f "${XCODEPATH}/Xcode" ]; then\
		echo "Can not find the Xcode.";\
		exit 1;\
	fi
	@rm -rf ${XCODETEMPLATEPATH}/${Template};
	@echo "Uninstall successed";

update::
	@if ! [[ $EUID -eq 0 ]]; then\
		echo "This script should be run using sudo or as the root user.";\
		exit 1;\
	fi
	make uninstall;
	make install;
	@echo "Update successed";
	
