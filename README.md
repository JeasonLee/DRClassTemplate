## 描述
`Xcode`自定义类模板文件，用于新建继承`DRRequest`的类模板。

## 原理

使用`make命令`将包含模板文件的`DRClass`文件夹复制到`Xcode`的模板目录下，实现模板插入。

## 使用
进入`Makefile`文件所在的目录后使用下列命令：

- `sudo make install` 安装
- `sudo make uninstall` 卸载
- `sudo make update` 更新

## 截图
![](Screenshot/0x01.png)
![](Screenshot/0x02.png)
![](Screenshot/0x03.png)
![](Screenshot/0x04.png)